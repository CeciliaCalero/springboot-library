

def call(Map parametros){
pipeline { 
    agent {
        kubernetes{
            label 'pipeline-springboot'
            yaml"""
        apiVersion: v1
        kind: Pod
        metadata:
          labels:
            some-label: some-label-name
        spec:
          containers:
          - name: maven
            image: maven:alpine
            command:
            - cat
            tty: true
          - name: docker
            image: docker:stable-dind
            command:
            - cat
            tty: true
            volumeMounts:
            - name: dockersock
              mountPath: "/var/run/docker.sock"
          - name: helm
            image: alpine/helm:latest
            command:
            - cat
            tty: true
          volumes:
          - name: dockersock
            hostPath:
              path: "/var/run/docker.sock"
          """  
        }
    }
      environment{
        

         enableBuild = "${parametros.ENABLE_BUILD}"


        registry="ceciliadcalero/appjava"
        imageName="ceciliadcalero/appjava"
        registryCredenciales='dockerhub'
         dockerfile    = "Dockerfile" 
        
       
      }
    stages {
      
        stage('Build') { 
             when {
                  environment name: 'enableBuild', value: 'true'
                }
            steps {  
             
                container('maven'){
                sh 'mvn clean package' 
            }
            }
      }
        stage('Test'){
            steps {
                container('maven'){
                    sh """ mvn test
                      ls `pwd`/target """
                    
                }
                
            }
        }
        stage('Sonar') {
            steps {
                container('maven') {
                    script{
                        scannerHome = tool 'SonarQubeScanner'
                      sh"${scannerHome}/bin/sonar-scanner"}
                 // sh'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.7.0.1746:sonar'
                
               // sh' mvn sonar:sonar -Dproject.settings=sonar-project.properties'
            // sh 'mvn sonar:sonar'
              
            }
            }
        }
      stage('Nexus') {
            steps {
                   container('maven') {
             sh 'mvn clean deploy -s .m2/settings.xml'
      //sh  "${mvnHome}/bin/mvn clean package"
           ///  sh 'mvn clean deploy'      
                  }
             }
      }
        stage('docker') {
            steps {
              container('docker'){
                  script{
                  def customImage= docker.build("${imageName}", "-f ${dockerfile} $WORKSPACE")
                    docker.withRegistry("", "${registryCredenciales}"){
                     customImage.push()
                    }
                  
                }
              }
              
            }
        }
          stage('Deploy helm') {
            steps {
              //helm upgrade -f.deploy/values.yaml -f .deploy/Chart.yaml appJava .deploy
               container('helm'){
                
                 sh """
                 cat .deploy/values.yaml
                 cat .deploy/Chart.yaml
                 helm upgrade -i --recreate-pods app .deploy
                 helm list
                 ls
                 pwd
                 """
              
            }
        }
      }
    }
}
}

